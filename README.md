**About this repository**

This is a repository containing my recent coding projects in Python. 


**About Cortical Circuit Model**

I developed the circuit Network Model 
using the Brian2 library for spiking neuronal networks in Python (see : https://brian2.readthedocs.io/en/stable/), during my postdoctoral term at the Banks Laboratory (http://anesthesia.wisc.edu/index.php?title=Banks_Laboratory), 
Dept. Anaesthesiology, UW-Madison (Oct 2015-Feb 2017). This code
is not for commercial use and is provided here with permission from Dr.Banks for educational purposes only.
The cortical circuit model code simulates a single cortical microcircuit
with the aim of replicating activity observed in the primary auditory cortex of the rat. 
The source contains comments elucidating the form and function of the network model, as
well as the tools used to record the simulated activity. 



** Projects in development **

Model of self-control in intertemporal choice. Decision support tool based on self-control model. 
This README will be updated as new projects are added. 
